\page UsersGuide Users' Guide

# Table of Content

## Basic topics

1. [General concepts](./GeneralConcepts/ug_GeneralConcepts.md)
2. [Arrays](./Arrays/ug_Arrays.md)
3. [Vectors](./Vectors/ug_Vectors.md)
4. [Flexible parallel reduction and scan](./ReductionAndScan/ug_ReductionAndScan.md)
5. [For loops](./ForLoops/ug_ForLoops.md)
6. [Sorting](./Sorting/ug_Sorting.md)
7. [Cross-device pointers](./Pointers/ug_Pointers.md)

## Data structures

1. [Multidimensional arrays](./NDArrays/ug_NDArrays.md)
2. [Matrices](./Matrices/ug_Matrices.md)
3. [Segments (sparse formats)](./Segments/ug_Segments.md)
4. [Orthogonal grids](./Meshes/ug_Grids.md)
5. [Unstructured meshes](./Meshes/ug_Meshes.md)

## Numerical solvers

1. [Linear solvers](./Solvers/Linear/ug_Linear_solvers.md)
2. [ODE solvers](./Solvers/ODE/ug_ODE_solvers.md)
